var en = {
    "select.list.of.lists": "Select List of Lists",
    "list.of.lists": "List of Lists",
    "new.list.of.lists": "New List of Lists",
    "logout": "Logout",
    "list.type": "List Type",
    "first.in.first.out": "First In First Out",
    "last.in.first.out": "Last In First Out",
    "save": "Save",
    "delete": "Delete",
    "add.list": "Add List",
    "all": "All",
    "complete": "Complete",
    "uncompleted": "Uncompleted",
    "created.on": "Created on",
    "list": "List",
    "enter": "ENTER",
    "new.list.item.is": "New list item is ...",

    "enter.tooltip": "Click here add to list",
    "all.tooltip": "Show all items",
    "completed.tooltip": "Show only completed items",
    "uncompleted.tooltip": "Show only uncompleted items",
    "fifo.tooltip": "Order items in ascending order by date",
    "lifo.tooltip": "Order items in descending order by date",

    "no.conn": "Could not connect to server"
};