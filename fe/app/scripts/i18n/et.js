var et = {
    "select.list.of.lists": "Vali nimekirjade nimekiri",
    "list.of.lists": "Nimekirjade nimekiri",
    "new.list.of.lists": "Uus nimekirjade nimekiri",
    "logout": "Logi välja",
    "list.type": "Nimekirja tüüp",
    "first.in.first.out": "Esimesena sisse, esimesena välja",
    "last.in.first.out": "Viimasena sisse, esimesena välja",
    "save": "Salvesta",
    "delete": "Kustuta",
    "add.list": "Lisa nimekiri",
    "all": "Kõik",
    "complete": "Valmis",
    "uncompleted": "Pooleli",
    "created.on": "Loodud",
    "list": "Nimekiri",
    "enter": "SISESTA",
    "new.list.item.is": "Uus kirje on ...",

    "enter.tooltip": "Vajuta siia, et lisada",
    "all.tooltip": "Näita kõiki kirjeid",
    "completed.tooltip": "Näita ainult valmis kirjeid",
    "uncompleted.tooltip": "Näita ainult pooleli kirjeid",
    "fifo.tooltip": "Järjesta kirjed ajas kasvavalt",
    "lifo.tooltip": "Järjesta kirjed ajas kahanevalt",

    "no.conn": "Katkes ühendus serveriga"
};