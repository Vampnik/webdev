'use strict';

angular
    .module('feApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.bootstrap',
        'ngConfirm',
        'pascalprecht.translate',
        'ngWebSocket',
        'cgNotify',
        'ui-notification'
    ])
    .factory('AppHttpInterceptor', ['$q', '$rootScope', '$location', function ($q, $rootScope, $location) {
        return {
            'request': function (config) {
                return config;
            },

            'response': function (response) {
                return response;
            },

            'responseError': function (response) {
                $rootScope.noConnection = false;
                if(response.status < 0) {
                    $rootScope.noConnection = true;
                }
                if(response.status === 401) {
                    if(window.location.href.match("sign$") != "sign") { // jshint ignore:line
                        $rootScope.redirectUrl = window.location.href;
                    }
                    $location.path("/sign");
                }
                return $q.reject(response);
            }
        };
    }])
    .config(function ($routeProvider, $httpProvider, $translateProvider) {
        $httpProvider.interceptors.push('AppHttpInterceptor');

        $translateProvider.translations('en', en);
        $translateProvider.translations('et', et);
        $translateProvider.preferredLanguage('en');
        //$translateProvider.useSanitizeValueStrategy('sanitize');

        $routeProvider
            .when('/', {
                templateUrl: 'views/home.html',
                controller: 'HomeCtrl',
                controllerAs: 'lol'
            })
            .when('/lol/:lolId', {
                templateUrl: 'views/list-of-lists.html',
                controller: 'ListOfListsCtrl',
                controllerAs: 'lol'
            })
            .when('/elist/:lolId/:listId', {
                templateUrl: 'views/edit-list.html',
                controller: 'EditListCtrl',
                controllerAs: 'elist'
            })
            .when('/elol/:lolId', {
                templateUrl: 'views/edit-list-of-lists.html',
                controller: 'EditListOfListsCtrl',
                controllerAs: 'elol'
            })
            .when('/list/:lolId/:listId', {
                templateUrl: 'views/list.html',
                controller: 'ListCtrl',
                controllerAs: 'list'
            })
            .when('/sign', {
                templateUrl: 'views/sign.html',
                controller: 'SignCtrl',
                controllerAs: 'sign'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
