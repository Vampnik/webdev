'use strict';

angular.module('feApp')
    .controller('ListCtrl', function (GlobalService, $routeParams, $http) {
        var ctrl = this;
        GlobalService.initGlobal(this, "list", function() {
            ctrl.load();
        });
        ctrl.go = GlobalService.go;
        ctrl.filter = "all";

        ctrl.lolId = $routeParams.lolId;
        ctrl.listId = $routeParams.listId;

        var save = function(toSave) {
            $http.post(apiRoot + "api/lol/" + ctrl.lolId + "/list/" + ctrl.listId + "/record", toSave).success(function() {
                ctrl.load();
            });
        };

        ctrl.enter = function() {
            save({value: ctrl.edit});
            ctrl.unselect();
        };

        ctrl.select = function(item) {
            ctrl.selected = item;
            ctrl.selectedEdit = item.value;
        };

        ctrl.saveSelected = function() {
            var toSave = JSON.parse(JSON.stringify(ctrl.selected));
            toSave.value = ctrl.selectedEdit;
            save(toSave);
            ctrl.unselect();
        };

        ctrl.unselect = function() {
            ctrl.selected = undefined;
            ctrl.selectedEdit = "";
            ctrl.edit = "";
        };

        ctrl.toggle = function(item) {
            var toSave = JSON.parse(JSON.stringify(item));
            toSave.completed = item.completed ? false : true;
            save(toSave);
        };


        ctrl.delete = function(item) {
            $http.delete(apiRoot + "api/lol/" + ctrl.lolId + "/list/" + ctrl.listId + "/record/" + item.id).success(function() {
                ctrl.load();
            });
        };

        ctrl.doFilter = function() {
            if(ctrl.filter === "completed") {
                ctrl.list = ctrl.completedList;
            } else if(ctrl.filter === "uncompleted") {
                ctrl.list = ctrl.uncompletedList;
            } else {
                ctrl.list = ctrl.allList;
            }
        };

        ctrl.load = function() {
            $http.get(apiRoot + "api/lol/" + ctrl.lolId + "/list/" + ctrl.listId + "/record").success(function(data) {
                ctrl.allList = data;
                ctrl.uncompletedList = [];
                ctrl.completedList = [];
                ctrl.allCount = data.length;
                ctrl.uncompletedCount = 0;
                ctrl.completedCount = 0;
                for(var i=0;i<data.length;i++) {
                    if(data[i].completed) {
                        ctrl.completedCount++;
                        ctrl.completedList.push(data[i]);
                    } else {
                        ctrl.uncompletedCount++;
                        ctrl.uncompletedList.push(data[i]);
                    }
                }
                ctrl.doFilter();
            });
        };

        ctrl.unselect();
        ctrl.load();

    });
