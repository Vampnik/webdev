'use strict';

angular.module('feApp')
    .controller('SignCtrl', function ($location, $rootScope) {
        $rootScope.view = "sign";
        var ctrl = this;

        if($rootScope.redirectUrl) {
            ctrl.redirUrl = $rootScope.redirectUrl;
        } else {
            ctrl.redirUrl = window.location.href + "/../";
        }

        ctrl.doSign = function () {
            //$rootScope.signed = true;
            //$location.path("/");
        };

        ctrl.apiRoot = apiRoot;
    });
