'use strict';

angular.module('feApp')
    .controller('EditListOfListsCtrl', function ($location, GlobalService, $http, $routeParams) {
        GlobalService.initGlobal(this, "elol");
        var ctrl = this;
        ctrl.lolId = $routeParams.lolId;

        if($routeParams.lolId === "add") {
            ctrl.edit = {type:"LIST_FIFO"};
        } else {
            $http.get(apiRoot + "api/lol/" + $routeParams.lolId).success(function(data) {
                ctrl.edit = data;
            });
        }

        ctrl.doSave = function () {
            $http.post(apiRoot + "api/lol", ctrl.edit).success(function() {
                $location.path("/");
            });
        };

        ctrl.delete = function () {
            $http.delete(apiRoot + "api/lol/" + $routeParams.lolId).success(function() {
                $location.path("/");
            });
        };
    });
