'use strict';

angular.module('feApp')
    .controller('ListOfListsCtrl', function ($http, GlobalService, $routeParams) {
        GlobalService.initGlobal(this, "lol");
        var ctrl = this;
        ctrl.go = GlobalService.go;
        ctrl.filter = "all";

        ctrl.lolId = $routeParams.lolId;

        ctrl.delete = function(list) {
            $http.delete(apiRoot + "api/lol/" + ctrl.lolId + "/list/" + list.id).success(function() {
                ctrl.load();
            });
        };

        ctrl.doFilter = function() {
            if(ctrl.filter === "completed") {
                ctrl.lists = ctrl.completedList;
            } else if(ctrl.filter === "uncompleted") {
                ctrl.lists = ctrl.uncompletedList;
            } else {
                ctrl.lists = ctrl.allList;
            }
        };

        ctrl.load = function() {
            $http.get(apiRoot + "api/lol/" + ctrl.lolId + "/list").success(function(data) {
                ctrl.allList = data;
                ctrl.uncompletedList = [];
                ctrl.completedList = [];
                ctrl.allCount = data.length;
                ctrl.uncompletedCount = 0;
                ctrl.completedCount = 0;
                for(var i=0;i<data.length;i++) {
                    if(data[i].completed) {
                        ctrl.completedCount++;
                        ctrl.completedList.push(data[i]);
                    } else {
                        ctrl.uncompletedCount++;
                        ctrl.uncompletedList.push(data[i]);
                    }
                }
                ctrl.doFilter();
            });
        };

        ctrl.load();

    });
