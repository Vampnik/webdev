'use strict';

angular.module('feApp')
    .controller('MenuCtrl', function (GlobalService) {
        var ctrl = this;
        ctrl.changeLanguage = GlobalService.changeLanguage;
    });
