'use strict';

angular.module('feApp')
    .controller('EditListCtrl', function ($location, GlobalService, $routeParams, $http) {
        GlobalService.initGlobal(this, "elist");
        var ctrl = this;

        ctrl.lolId = $routeParams.lolId;
        ctrl.listId = $routeParams.listId;

        if($routeParams.listId === "add") {
            ctrl.edit = {type:"LIST_FIFO"};
        } else {
            $http.get(apiRoot + "api/lol/" + $routeParams.lolId + "/list/" + $routeParams.listId).success(function(data) {
                ctrl.edit = data;
            });
        }

        ctrl.doSave = function () {
            $http.post(apiRoot + "api/lol/" + $routeParams.lolId + "/list", ctrl.edit).success(function() {
                $location.path("/lol/" + ctrl.lolId);
            });
        };
    });
