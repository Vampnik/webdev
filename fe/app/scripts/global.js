'use strict';

angular.module('feApp')
    .factory('GlobalService', ['$rootScope', '$http', '$location', '$translate', '$websocket', function ($rootScope, $http, $location, $translate, $websocket) {
        return {
            initGlobal: function(ctrl, view, onNotify) {
                $rootScope.view = view;
                ctrl.changeLanguage = this.changeLanguage;
                ctrl.go = this.go;
                $http.get(apiRoot + "api/profile").success(function(data) {
                    $rootScope.profile = data;
                    $rootScope.signed = true;
                    $translate.use(data.locale);
                });
                $http.get(apiRoot + "api/lol").success(function(data) {
                    $rootScope.lols = data;
                    $rootScope.lolMap = {};
                    for(var i=0;i<data.length;i++) {
                        $rootScope.lolMap[data.id] = data;
                    }
                });
                if(onNotify) {
                    var dataStream = $websocket("ws://localhost:8080/notify/websocket");
                    dataStream.onMessage(function(message) {
                        onNotify(JSON.parse(message.data));
                    });
                }
            },
            go: function (path) {
                $location.path(path);
            },
            changeLanguage: function (key) {
                $translate.use(key);
                $http.post(apiRoot + "api/locale/" + key, undefined);
            }


    };
    }]);