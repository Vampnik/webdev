Given(/^Lizt is opened$/) do
  visit $site_url
end

Then(/^click on "([^"]*)" button$/) do |arg1|
  click_button("#{arg1}")
end

Then(/^click on "([^"]*)" link$/) do |arg1|
  #click_link("#{arg1}")
  first("a", :text => "#{arg1}").click
  #first(:link, $arg1).click
  sleep(2)
end

Then(/^open dropdown "([^"]*)"$/) do |arg1|
  #click_link("#{arg1}")
  first("a", :text => "#{arg1}").click
  #first(:link, $arg1).click
  sleep(2)
end

Then(/^click on "([^"]*)" "([^"]*)"$/) do |arg1, arg2|
  first("#{arg2}", :text => "#{arg1}").click
  sleep(2)
end

Then(/^fill into "([^"]+)" value "([^"]+)"$/) do |arg1, arg2|
  #save_and_open_page
  #first(:id, "#{arg1}").set("#{arg2}")
  fill_in("#{arg1}", :with => "#{arg2}")
end

Then(/^Insert credentials$/) do
  fill_in("email", :with => $username)
  fill_in("pass", :with => $password)
  click_button("Log In")
end

