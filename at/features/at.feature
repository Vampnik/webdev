@Main
Feature: Test Some

  @simple
  Scenario: Simple Scenario
    Given Lizt is opened
    Then click on "Use Facebook" button
    And Insert credentials
    And click on "EN" link
    And open dropdown "Select List of Lists"
    And click on "New List of Lists" link
    And fill into "lolname" value "Super List"
    And click on "Last In First Out" "label"
    And click on "Save" link
    And click on "Super List" link
    And click on "Add List" link
    And fill into "lname" value "Minor List"
    And click on "Save" link
    And click on "Minor List" "span"
    And fill into "newitem" value "Some Item"
    And click on "ENTER" link
