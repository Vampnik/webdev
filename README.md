# Project for Web Development course

by Tõnis Ojandu

## Test server

http://ojandu.eu/lizt

## Dockerhub

* Front-end [vampnik/webdev-fe](https://hub.docker.com/r/vampnik/webdev-fe/)
* Back-end [vampnik/webdev-rest](https://hub.docker.com/r/vampnik/webdev-rest/)

It is recomended to use [Docker Compose](https://docs.docker.com/compose/install/) when deploying yourself. Copy `sample-docker-compose.yml` to `docker-compose.yml`.
Edit `docker-compose.yml` to have your `FB_APP_ID` and `FB_APP_SECRET`. Also if you prefer to use different ports for `nginx` then also change `PUBLIC_HOST`.
Then run:

```
docker-compose pull
docker-compose up -d
```

# Prequisites for Building

* Java 8
* Maven
* Docker
* Docker Compose

# Running Your Build

Run:

```
mvn clean install && docker-compose up -d
```

and open <http://localhost:8080>

# Development Phases for Grading

## Phase Four (2016-04-10):

### "Võtted"
* 1.1.4 Javascriptiga lehe muutmine
* 1.1.6 Interaktiivne abiinfo
* 1.2.6 Võrguühenduseta kasutatav
* 1.2.8 Mitmekeelne kasutajaliides

## Phase Three (2016-03-27):

### "Võtted"
* 2.1 Funktsionaalsus
* 1.2.4 AJAX
* 1.1.1 Andmed andmebaasis
* 1.1.7 AJAX viited
* 1.2.9 Autoriseerimine

## Phase Two (2016-03-06): 

### "Võtted"

* 1.3.6 [Projektiplaan](https://bitbucket.org/Vampnik/webdev/wiki/Projekti_plaan) 
* 1.3.5 [Rakendus testserveris](http://ojandu.eu/lizt)
* 1.2.3 Autentimine Facebookiga
* 1.1.8 Melimise järel naastakse õigele lehele

## Phase One (2016-02-14): 

### "Võtted"

* 1.2.10 Prototüüp:

### How To Access 

There are two ways to access the solution for this phase:

* See "Running" part of this document
* Using NPM and Grunt. Run:

```
cd fe
npm install
grunt serve
```

and open <http://localhost:9000>