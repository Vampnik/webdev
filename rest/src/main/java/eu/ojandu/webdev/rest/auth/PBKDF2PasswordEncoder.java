package eu.ojandu.webdev.rest.auth;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

public class PBKDF2PasswordEncoder {
    public static String encode(CharSequence cs) {
        try {
            return PasswordHash.createHash(cs.toString());
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        } catch (InvalidKeySpecException ex) {
            throw new RuntimeException(ex);
        }
    }

    public static boolean matches(CharSequence cs, String string) {
        try {
            return PasswordHash.validatePassword(cs.toString(), string);
        } catch (NoSuchAlgorithmException ex) {
            throw new RuntimeException(ex);
        } catch (InvalidKeySpecException ex) {
            throw new RuntimeException(ex);
        }
    }
}