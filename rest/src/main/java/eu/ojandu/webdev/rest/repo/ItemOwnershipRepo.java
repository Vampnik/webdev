package eu.ojandu.webdev.rest.repo;

import eu.ojandu.webdev.rest.model.ItemOwnership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemOwnershipRepo extends JpaRepository<ItemOwnership, Long> {

    List<ItemOwnership> getByOwnerId(long id);

    ItemOwnership getByOwnerIdAndItemId(long ownerId, long itemId);

    @Modifying
    @Query("delete from ItemOwnership where itemId=?1")
    void removeOwnershipByItemId(long itemId);
}
