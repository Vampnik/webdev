package eu.ojandu.webdev.rest.domain;

import java.util.HashMap;

public class ResponseMap extends HashMap<String, Object> implements RestResponse {

    @Override
    public ResponseMap put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
