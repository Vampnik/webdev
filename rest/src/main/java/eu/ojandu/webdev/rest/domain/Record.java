package eu.ojandu.webdev.rest.domain;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

@Data
public class Record {

    private Long id;
    @NotEmpty
    private String value;

    private Boolean completed;
}
