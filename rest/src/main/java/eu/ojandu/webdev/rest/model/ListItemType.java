package eu.ojandu.webdev.rest.model;

public enum ListItemType {
    LIST_LIFO(false),
    LIST_FIFO(false),
    TEXT(true);

    private boolean leaf;

    ListItemType(boolean leaf) {
        this.leaf = leaf;
    }

    public boolean isLeaf() {
        return leaf;
    }
}
