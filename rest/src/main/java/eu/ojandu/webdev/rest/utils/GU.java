package eu.ojandu.webdev.rest.utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import eu.ojandu.webdev.rest.domain.*;
import org.apache.commons.io.IOUtils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.security.Principal;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GU {

    public static final ObjectMapper objectMapper = new ObjectMapper();

    public static ResponseMap buildMap() {
        return new ResponseMap();
    }

    public static ResponseList buildList() {
        return new ResponseList();
    }

    public static MultikeyMap presumeMap(Object raw) {
        if(raw == null) {
            throw new IllegalArgumentException("expected map but got null");
        }
        if(!(raw instanceof Map)) {
            throw new IllegalArgumentException("expected map but got " + raw.getClass());
        }
        return new MultikeyMap((Map) raw);
    }

    public static MultikeyMap presumeMap(HttpResponse<JsonNode> response) {
        try {
            HashMap map = objectMapper.readValue(IOUtils.toString(response.getRawBody()), new TypeReference<HashMap>() {});
            return new MultikeyMap(map);
        } catch (Exception e) {
            throw new IllegalArgumentException("Could not produce multikey map: ", e);
        }
    }

    public static ResponseMap asMap(Object object) {
        ResponseMap map = new ResponseMap();

        for(Field field : object.getClass().getDeclaredFields()) {
            boolean ignore = false;
            for(Annotation annotation : field.getDeclaredAnnotations()) {
                if(annotation instanceof DontSerialize) {
                    ignore = true;
                }
            }
            if(!ignore) {
                boolean accessibility = field.isAccessible();
                try {
                    field.setAccessible(true);
                    Object value = field.get(object);
                    if(value != null) {
                        map.put(field.getName(), value);
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Could not access. Fix code!");
                } finally {
                    field.setAccessible(accessibility);
                }
            }
        }
        return map;
    }

    public static void apply(Map<String, Object> map, Object object) {
        Set<String> keys = map.keySet();
        for(Field field : object.getClass().getDeclaredFields()) {
            boolean ignore = false;
            for(Annotation annotation : field.getDeclaredAnnotations()) {
                if(annotation instanceof DontSerialize) {
                    ignore = true;
                }
            }
            if(!ignore && keys.contains(field.getName())) {
                boolean accessibility = field.isAccessible();
                try {
                    field.setAccessible(true);
                    Object value = field.get(object);
                    if(value != null) {
                        field.set(object, map.get(field.getName()));
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException("Could not access. Fix code!");
                } finally {
                    field.setAccessible(accessibility);
                }
            }
        }
    }

    public static <T> RestResponse resp(T[] ary) {
        ResponseList list = buildList();
        ast(ary).forEach(list::add);
        return list;
    }

    public static RestResponse resp(Collection collection) {
        ResponseList list = buildList();
        collection.forEach(list::add);
        return list;
    }

    public static <T> Stream<T> ast(T[] ary) {
        return Arrays.stream(ary);
    }

    public static <I, T> Map<I, T> mapBy(Function<T, I> by, Collection<T> col) {
        return col.stream().collect(Collectors.toMap(by, t -> t));
    }

    public static PrincipalUser p(Principal principal) {
        return (PrincipalUser)principal;
    }
}
