package eu.ojandu.webdev.rest.conf;

import eu.ojandu.webdev.rest.domain.ActiveUser;
import eu.ojandu.webdev.rest.domain.PrincipalUser;
import eu.ojandu.webdev.rest.service.SocialService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;


public class CurrentUserHandlerMethodArgumentResolver
        implements HandlerMethodArgumentResolver {

    private static Logger log = LoggerFactory.getLogger(CurrentUserHandlerMethodArgumentResolver.class);
    private ApplicationContext applicationContext;

    public CurrentUserHandlerMethodArgumentResolver() {
        log.info("asd: CurrentUserHandlerMethodArgumentResolver created: " + this);
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    private SocialService socialService;

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        log.info("/: " + methodParameter + " " + (methodParameter.getParameterAnnotation(ActiveUser.class) != null
                && methodParameter.getParameterType().equals(PrincipalUser.class)));
        return
                methodParameter.getParameterAnnotation(ActiveUser.class) != null
                        && methodParameter.getParameterType().equals(PrincipalUser.class);
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        log.info("Resolving princ user: " + this.supportsParameter(methodParameter) + " " + methodParameter);
        if (this.supportsParameter(methodParameter)) {
            log.info("Is supported: " + socialService.getUser());
            return socialService.getUser();
        } else {
            log.info("Is not supported: " + WebArgumentResolver.UNRESOLVED);
            return WebArgumentResolver.UNRESOLVED;
        }
    }

    public void init() {
        this.socialService = applicationContext.getBean(SocialService.class);
    }
}