package eu.ojandu.webdev.rest.repo;

import eu.ojandu.webdev.rest.model.ListItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ListItemRepo extends JpaRepository<ListItem, Long> {

    List<ListItem> getByIdIn(List<Long> id);

    List<ListItem> getByParentItemId(long parentItemId);

    @Query("select count(id) from ListItem where completed <> true and parentItemId = ?1")
    long getUncompletedChildrenCount(long parentItemId);
}
