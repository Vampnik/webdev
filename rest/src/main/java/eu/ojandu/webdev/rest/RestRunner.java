package eu.ojandu.webdev.rest;

import eu.ojandu.webdev.rest.conf.AuthConfiguration;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
//@ComponentScan("eu.ojandu.webdev.rest")
@ComponentScan
@EnableAutoConfiguration
@EnableTransactionManagement(proxyTargetClass = true)
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
@EnableAspectJAutoProxy
public class RestRunner {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(new Object[]{RestRunner.class, AuthConfiguration.class}, args);
    }
}