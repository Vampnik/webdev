package eu.ojandu.webdev.rest.controller;

import eu.ojandu.webdev.rest.aop.NotifyClients;
import eu.ojandu.webdev.rest.domain.*;
import eu.ojandu.webdev.rest.exceptions.UnauthorizedException;
import eu.ojandu.webdev.rest.model.ItemOwnership;
import eu.ojandu.webdev.rest.model.ListItem;
import eu.ojandu.webdev.rest.model.ListItemType;
import eu.ojandu.webdev.rest.model.OwnershipType;
import eu.ojandu.webdev.rest.repo.ItemOwnershipRepo;
import eu.ojandu.webdev.rest.repo.ListItemRepo;
import static eu.ojandu.webdev.rest.utils.GU.*;

import eu.ojandu.webdev.rest.service.SocialService;
import eu.ojandu.webdev.rest.utils.GU;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@RestController
public class ListCtrl {

    @Autowired
    private ItemOwnershipRepo itemOwnershipRepo;

    @Autowired
    private ListItemRepo listItemRepo;

    @Autowired
    private SocialService socialService;

    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol", method = RequestMethod.GET)
    public RestResponse getListsOfLists(@ActiveUser PrincipalUser principalUser) {
        principalUser = socialService.getUser();
        List<ItemOwnership> ownerships = itemOwnershipRepo.getByOwnerId(principalUser.getId());
        List<ListItem> items = listItemRepo.getByIdIn(ownerships.stream().map(ItemOwnership::getItemId).collect(Collectors.toList()));
        Map<Long, ItemOwnership> ownershipMap = mapBy(o -> o.getItemId(), ownerships);

        return resp(items.stream().map(item -> buildMap()
                .put("id", item.getId())
                .put("name", item.getValue())
                .put("type", item.getType())
                .put("access", ownershipMap.get(item.getId()).getOwnershipType().name())
        ).collect(Collectors.toList()));
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{id}", method = RequestMethod.GET)
    public RestResponse getListOfLists(@ActiveUser PrincipalUser principalUser, @PathVariable("id") long id) {
        principalUser = socialService.getUser();
        ListItem item = listItemRepo.getOne(id);
        getOwnership(principalUser, item).canRead();
        return buildMap()
                .put("id", item.getId())
                .put("name", item.getValue())
                .put("type", item.getType())
                .put("access", itemOwnershipRepo.getByOwnerIdAndItemId(principalUser.getId(), id).getOwnershipType().name());
    }

    @NotifyClients
    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol", method = RequestMethod.POST)
    public void saveOrUpdateListOfList(@Valid @RequestBody ListOfLists dto, @ActiveUser PrincipalUser principalUser) {
        principalUser = socialService.getUser();
        ListItem item = new ListItem();
        item.setCreatedOn(System.currentTimeMillis());
        boolean isNew = true;
        if(dto.getId() != null && dto.getId() > 0) {
            item = getRootItem(dto.getId());
            getOwnership(principalUser, item).canWrite();
            isNew = false;
        }
        if(dto.getType().isLeaf()) {
            throw new IllegalArgumentException("only LIFO/FIFO_LIST allowed");
        }
        item.setValue(dto.getName());
        item.setType(dto.getType());
        item = listItemRepo.save(item);
        if(isNew) {
            ItemOwnership itemOwnership = new ItemOwnership();
            itemOwnership.setItemId(item.getId());
            itemOwnership.setOwnerId(principalUser.getId());
            itemOwnership.setOwnershipType(OwnershipType.ADMIN);
            itemOwnershipRepo.save(itemOwnership);
        }
    }

    @NotifyClients
    @Transactional
    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{id}", method = RequestMethod.DELETE)
    public void get(@ActiveUser PrincipalUser principalUser, @PathVariable("id") long id) {
        principalUser = socialService.getUser();
        ListItem item = getRootItem(id);
        getOwnership(principalUser, item).canWrite();
        itemOwnershipRepo.removeOwnershipByItemId(item.getId());
    }

    private ListItem getRootItem(@PathVariable("id") long id) {
        ListItem item = listItemRepo.getOne(id);
        if(item == null || item.getParentItemId() != null) {
            throw new UnauthorizedException();
        }
        return item;
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{id}/list", method = RequestMethod.GET)
    public RestResponse getLists(@ActiveUser PrincipalUser principalUser, @PathVariable("id") long id) {
        principalUser = socialService.getUser();
        ListItem item = getRootItem(id);
        getOwnership(principalUser, item).canRead();
        ListItem[] items = listItemRepo.getByParentItemId(item.getId()).stream().toArray(ListItem[]::new);

        long direction = item.getType().equals(ListItemType.LIST_FIFO) ? 1 : -1;
        Arrays.sort(items, (a, b) -> (int)(direction * (a.getCreatedOn() - b.getCreatedOn())));

        return resp(ast(items).map(i -> GU.buildMap()
                .put("id", i.getId())
                .put("name", i.getValue())
                .put("type", i.getType())
                .put("createdOn", i.getCreatedOn())
                .put("completed", listItemRepo.getUncompletedChildrenCount(i.getId()) == 0)
        ).collect(Collectors.toList()));
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{lolId}/list/{listId}", method = RequestMethod.GET)
    public RestResponse getList(@ActiveUser PrincipalUser principalUser, @PathVariable("lolId") long lolId, @PathVariable("listId") long listId) {
        principalUser = socialService.getUser();
        ListItem item = listItemRepo.getOne(listId);
        getOwnership(principalUser, item).canRead();
        return GU.buildMap()
                .put("id", item.getId())
                .put("name", item.getValue())
                .put("type", item.getType())
                .put("createdOn", item.getCreatedOn())
                .put("completed", listItemRepo.getUncompletedChildrenCount(item.getId()) == 0);
    }

    @NotifyClients
    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{id}/list", method = RequestMethod.POST)
    public void saveOrUpdateList(@ActiveUser PrincipalUser principalUser, @PathVariable("id") long id, @Valid @RequestBody SList list) {
        principalUser = socialService.getUser();
        ListItem listItem = new ListItem();
        listItem.setCreatedOn(System.currentTimeMillis());
        listItem.setParentItemId(id);
        if(list.getId() != null) {
            listItem = listItemRepo.getOne(list.getId());
            if(listItem == null) {
                throw new UnauthorizedException();
            }
            getOwnership(principalUser, listItem).canWrite();
        }
        listItem.setValue(list.getName());
        if(list.getType().isLeaf()) {
            throw new IllegalArgumentException("only LIFO/FIFO_LIST allowed");
        }
        listItem.setType(list.getType());
        listItemRepo.save(listItem);
    }

    @NotifyClients
    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{lolId}/list/{listId}", method = RequestMethod.DELETE)
    public void deleteList(@ActiveUser PrincipalUser principalUser, @PathVariable("lolId") long lolId, @PathVariable("listId") long listId) {
        principalUser = socialService.getUser();
        ListItem listItem = listItemRepo.getOne(listId);
        getOwnership(principalUser, listItem).canWrite();
        listItem.setParentItemId(null);
        listItemRepo.save(listItem);
    }


    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{lolId}/list/{listId}/record", method = RequestMethod.GET)
    public RestResponse getRecords(@ActiveUser PrincipalUser principalUser, @PathVariable("lolId") long lolId, @PathVariable("listId") long listId) {
        principalUser = socialService.getUser();
        ListItem item = listItemRepo.getOne(listId);
        getOwnership(principalUser, item).canRead();
        ListItem[] items = listItemRepo.getByParentItemId(item.getId()).stream().toArray(ListItem[]::new);

        long direction = item.getType().equals(ListItemType.LIST_FIFO) ? 1 : -1;
        Arrays.sort(items, (a, b) -> (int)(direction * (a.getCreatedOn() - b.getCreatedOn())));

        return resp(ast(items).map(i -> GU.buildMap()
                .put("id", i.getId())
                .put("value", i.getValue())
                .put("completed", i.getCompleted() == null ? false : i.getCompleted())
                .put("createdOn", i.getCreatedOn())
        ).collect(Collectors.toList()));
    }

    @NotifyClients
    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{lolId}/list/{listId}/record", method = RequestMethod.POST)
    public void saveOrUpdateRecord(
            @ActiveUser PrincipalUser principalUser,
            @PathVariable("lolId") long lolId,
            @PathVariable("listId") long listId,
            @Valid @RequestBody Record record
    ) {
        principalUser = socialService.getUser();
        ListItem listItem = new ListItem();
        listItem.setCreatedOn(System.currentTimeMillis());
        listItem.setParentItemId(listId);
        listItem.setType(ListItemType.TEXT);
        if(record.getId() != null) {
            listItem = listItemRepo.getOne(record.getId());
            if(listItem == null) {
                throw new UnauthorizedException();
            }
            getOwnership(principalUser, listItem).canWrite();
        }
        listItem.setValue(record.getValue());
        listItem.setCompleted(record.getCompleted());
        listItemRepo.save(listItem);
    }

    @NotifyClients
    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/lol/{lolId}/list/{listId}/record/{rid}", method = RequestMethod.DELETE)
    public void deleteRecord(
            @ActiveUser PrincipalUser principalUser,
            @PathVariable("lolId") long lolId,
            @PathVariable("listId") long listId,
            @PathVariable("rid") long rid
    ) {
        principalUser = socialService.getUser();
        ListItem listItem = listItemRepo.getOne(rid);
        getOwnership(principalUser, listItem).canWrite();
        listItem.setParentItemId(null);
        listItemRepo.save(listItem);
    }


    private OwnershipType getOwnership(PrincipalUser principalUser, ListItem item) {
        if(item == null) {
            throw new UnauthorizedException();
        }
        if(item.getParentItemId() == null) {
            ItemOwnership own = itemOwnershipRepo.getByOwnerIdAndItemId(principalUser.getId(), item.getId());
            if(own == null) {
                return OwnershipType.NONE;
            }
            return own.getOwnershipType();
        } else {
            return getOwnership(principalUser, listItemRepo.findOne(item.getParentItemId()));
        }
    }
}
