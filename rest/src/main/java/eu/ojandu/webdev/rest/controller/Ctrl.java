package eu.ojandu.webdev.rest.controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import eu.ojandu.webdev.rest.domain.Locale;
import eu.ojandu.webdev.rest.domain.PrincipalUser;
import eu.ojandu.webdev.rest.domain.RestResponse;
import eu.ojandu.webdev.rest.model.WebUser;
import eu.ojandu.webdev.rest.repo.WebUserRepo;
import eu.ojandu.webdev.rest.service.Serv;
import eu.ojandu.webdev.rest.service.SocialService;
import eu.ojandu.webdev.rest.utils.GU;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
public class Ctrl {

    @Autowired
    private Serv serv;

    @Autowired
    private SocialService socialService;

    @Autowired
    private WebUserRepo webUserRepo;

    @RequestMapping("/api/hello")
    public RestResponse hello() {
        return GU.buildMap().put("hello", serv.getHello());
    }

    @RequestMapping(value = "/api/profile", method = RequestMethod.GET)
    public RestResponse profile() throws UnirestException, IOException {
        PrincipalUser profile = socialService.getUser();
        return GU.buildMap()
                .put("name", profile.getName())
                .put("locale", profile.getRecord().getStatus() == null ? Locale.en : profile.getRecord().getStatus());
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/api/locale/{locale}", method = RequestMethod.POST)
    public void setLocale(@PathVariable Locale locale) {
        WebUser user = webUserRepo.getOne(socialService.getUser().getId());
        user.setStatus(locale.name());
        socialService.getUser().getRecord().setStatus(locale.name());
        webUserRepo.save(user);
    }






}
