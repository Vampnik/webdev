package eu.ojandu.webdev.rest.aop;

import java.util.Date;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class NotifyAspect {

    @Autowired
    private SimpMessagingTemplate template;

    //private static final String WEBSOCKET_TOPIC = "/topic/notify";
    private static final String WEBSOCKET_TOPIC = "/notify";

    @After("@annotation(eu.ojandu.webdev.rest.aop.NotifyClients)")
    public void notifyClients() throws Throwable {
        System.out.println("ASDASD : ");
        template.convertAndSend(WEBSOCKET_TOPIC, new Date());
    }

}