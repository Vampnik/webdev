package eu.ojandu.webdev.rest.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ItemOwnership {

    @Id
    @GeneratedValue
    private Long id;

    private Long ownerId;

    private Long itemId;

    @Column
    @Enumerated(EnumType.STRING)
    private OwnershipType ownershipType;

}
