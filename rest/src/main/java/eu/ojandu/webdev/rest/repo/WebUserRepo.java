package eu.ojandu.webdev.rest.repo;

import eu.ojandu.webdev.rest.model.WebUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebUserRepo extends JpaRepository<WebUser, Long> {

    WebUser getByEmail(String email);

    WebUser getByFbId(String id);

}
