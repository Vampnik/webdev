package eu.ojandu.webdev.rest.domain;

import eu.ojandu.webdev.rest.model.WebUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.Collection;

public class PrincipalUser implements Authentication {
    private long id;
    private Collection<? extends GrantedAuthority> authorities;
    private WebUser record;
    private String fbToken;

    public PrincipalUser() {
    }

    public PrincipalUser(WebUser record, String fbToken) {
        this.id = record.getId();
        this.authorities = Arrays.asList(new SimpleGrantedAuthority("ROLE_USER"));
                //Arrays.stream(UserRole.valueOf(record.getRole()).getPrivileges())
                //.map(p -> new SimpleGrantedAuthority(p.name()))
                //.collect(Collectors.toList());
        this.record = record;
        this.fbToken = fbToken;
    }

    public long getId() {
        return id;
    }

    public WebUser getRecord() {
        return record;
    }

    @Override
    public String getName() {
        return record.getEmail();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public Object getCredentials() {
        return fbToken;
    }

    @Override
    public Object getDetails() {
        return "no details";
    }

    @Override
    public Object getPrincipal() {
        return getName();
    }

    @Override
    public boolean isAuthenticated() {
        return true;
    }

    @Override
    public void setAuthenticated(boolean b) throws IllegalArgumentException {

    }

    @Override
    public String toString() {
        return "PrincipalUser{" +
                "id=" + id +
                ", authorities=" + authorities +
                ", record=" + record +
                ", fbToken='" + fbToken + '\'' +
                '}';
    }
}
