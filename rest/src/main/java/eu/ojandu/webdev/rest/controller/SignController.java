package eu.ojandu.webdev.rest.controller;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import eu.ojandu.webdev.rest.domain.MultikeyMap;
import eu.ojandu.webdev.rest.domain.PrincipalUser;
import eu.ojandu.webdev.rest.model.WebUser;
import eu.ojandu.webdev.rest.repo.WebUserRepo;
import eu.ojandu.webdev.rest.service.SocialService;
import eu.ojandu.webdev.rest.utils.GU;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Controller
public class SignController {

    private static Logger log = LoggerFactory.getLogger(SignController.class);

    @Autowired
    private WebUserRepo webUserRepo;

    @Value("${public.host}")
    private String publicHost;

    @Value("${spring.social.facebook.appId}")
    private String fbAppId;

    @Value("${spring.social.facebook.appSecret}")
    private String fbAppSecret;

    @Autowired
    private SocialService socialService;

    @RequestMapping(value = "/api/connect/fb")
    public String connect(@RequestParam("redirect_uri") String redirectUri) throws UnsupportedEncodingException {
        try {
            return "redirect:https://www.facebook.com/dialog/oauth?client_id=" + fbAppId + "&redirect_uri=" +
                    URLEncoder.encode(publicHost + "api/connect/confirm/" + Base64.encode(redirectUri.getBytes()) + "/", "UTF-8");
        }catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @RequestMapping(value = "/api/connect/confirm/{encodedRedir}/")
    public String mem(@PathVariable("encodedRedir") String encodedRedir, @RequestParam("code") String code) throws UnirestException, IOException {
        try {
            String redirectUri = new String(Base64.decode(encodedRedir));
            MultikeyMap map = GU.presumeMap(Unirest.get("https://graph.facebook.com/v2.3/oauth/access_token?client_id=" + fbAppId + "&redirect_uri=" +
                    URLEncoder.encode(publicHost + "api/connect/confirm/" + Base64.encode(redirectUri.getBytes()) + "/", "UTF-8") +
                    "&client_secret=" + fbAppSecret + "&code=" + code)
                    .asJson());
            if (map.getString("access_token") == null) {
                log.error("Could not obtain token: " + map);
            } else {
                String token = map.getString("access_token");
                MultikeyMap profile = socialService.getFbProfile(token);
                log.info("Sign in with profile: " + profile);
                WebUser webUser = webUserRepo.getByFbId(profile.getNotNullString("id"));
                if (webUser == null) {
                    webUser = new WebUser();
                    webUser.setFbId(profile.getNotNullString("id"));
                }
                webUser.setName(profile.getString("name"));
                webUser.setEmail(profile.getString("email"));
                webUserRepo.save(webUser);

                PrincipalUser principalUser = new PrincipalUser(webUser, token);

                SecurityContextHolder.getContext().setAuthentication(principalUser);
                log.info("Setting principal use: " + token + " " + principalUser);
                socialService.set(token, principalUser);
            }
            return "redirect:" + redirectUri;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
