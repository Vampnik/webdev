package eu.ojandu.webdev.rest.domain;

import eu.ojandu.webdev.rest.model.ListItemType;
import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

@Data
public class SList {

    private Long id;
    @NotEmpty
    private String name;
    @NotNull
    private ListItemType type;
}
