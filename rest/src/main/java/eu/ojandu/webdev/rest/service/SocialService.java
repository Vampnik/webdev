package eu.ojandu.webdev.rest.service;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import eu.ojandu.webdev.rest.domain.MultikeyMap;
import eu.ojandu.webdev.rest.domain.PrincipalUser;
import eu.ojandu.webdev.rest.exceptions.UnauthorizedException;
import eu.ojandu.webdev.rest.utils.GU;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import java.io.IOException;


@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SocialService {

    private String fbAuthToken = null;
    private PrincipalUser user = null;

    public void set(String fbAuthToken, PrincipalUser user) {
        synchronized (this) {
            this.fbAuthToken = fbAuthToken;
            this.user = user;
        }
    }

    private String getFbAuthToken() {
        synchronized (this) {
            if(fbAuthToken == null) {
                throw new UnauthorizedException();
            }
            return fbAuthToken;
        }
    }

    public PrincipalUser getUser() {
        synchronized (this) {
            if(user == null) {
                throw new UnauthorizedException();
            }
            return user;
        }
    }

    public void signOut() {
        synchronized (this) {
            this.fbAuthToken = null;
            this.user = null;
        }
    }

    public MultikeyMap getFbProfile(String token) throws UnirestException, IOException {
        return GU.presumeMap(Unirest.get("https://graph.facebook.com/v2.5/me?fields=id,email,name&access_token=" + token)
                .asJson());
    }

    public MultikeyMap getFbProfile() throws UnirestException, IOException {
        return getFbProfile(getFbAuthToken());
    }
}
