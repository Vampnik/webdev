package eu.ojandu.webdev.rest.controller;

import eu.ojandu.webdev.rest.domain.ResponseMap;
import eu.ojandu.webdev.rest.exceptions.UnauthorizedException;
import eu.ojandu.webdev.rest.utils.GU;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.security.access.AccessDeniedException;

@ControllerAdvice
public class ErrorResponseAdvice {

    private static Logger log = LoggerFactory.getLogger(ErrorResponseAdvice.class);

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {AccessDeniedException.class})
    public
    @ResponseBody
    ResponseMap handleAccessDeniedError(AccessDeniedException e) {
        return handleAnyError(e);
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(value = {UnauthorizedException.class})
    public
    @ResponseBody
    ResponseMap handleUnauthorizedError(UnauthorizedException e) {
        return handleAnyError(e);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value = {Exception.class})
    public
    @ResponseBody
    ResponseMap handleAnyError(Exception e) {
        log.error("Received exception: ", e);
        return GU.buildMap()
                .put("exception", e.getClass().toString());
    }
}
