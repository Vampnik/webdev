package eu.ojandu.webdev.rest.domain;

/**
 * Created by ojandu on 4/7/16.
 */
public enum Locale {
    et,
    en
}
