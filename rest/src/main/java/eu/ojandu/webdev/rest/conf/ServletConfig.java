package eu.ojandu.webdev.rest.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.Lifecycle;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

@Configuration
public class ServletConfig extends WebMvcConfigurerAdapter implements ApplicationContextAware, Lifecycle {

    private static Logger log = LoggerFactory.getLogger(ServletConfig.class);

    private boolean running = false;

    public ServletConfig() {
        log.info("asd: ServletConfig created");
    }

    private CurrentUserHandlerMethodArgumentResolver currentUserResolver = new CurrentUserHandlerMethodArgumentResolver();

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        log.info("Setting current user resolver: " + currentUserResolver);
        //argumentResolvers.add(currentUserResolver);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        currentUserResolver.setApplicationContext(applicationContext);;
    }

    @Override
    public void start() {
        currentUserResolver.init();
        running = true;
    }

    @Override
    public void stop() {
        running = false;
    }

    @Override
    public boolean isRunning() {
        return running;
    }
}