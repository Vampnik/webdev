package eu.ojandu.webdev.rest.model;

import eu.ojandu.webdev.rest.utils.DontSerialize;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class WebUser {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private String email;

    @DontSerialize
    private String status;

    @DontSerialize
    private String passwordHash;

    @DontSerialize
    private String role;

    @DontSerialize
    private String fbId;

}
