package eu.ojandu.webdev.rest.model;

import eu.ojandu.webdev.rest.exceptions.UnauthorizedException;

public enum OwnershipType {
    ADMIN(3),
    WRITE(2),
    READ(1),
    NONE(0);

    private int rank;

    OwnershipType(int rank) {
        this.rank = rank;
    }

    public int getRank() {
        return rank;
    }

    public void canWrite() {
        if(rank < 2) {
            throw new UnauthorizedException();
        }
    }

    public void canRead() {
        if(rank < 1) {
            throw new UnauthorizedException();
        }
    }
}
