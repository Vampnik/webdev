package eu.ojandu.webdev.rest.domain;


import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;

public class MultikeyMap {

    private Map map;

    public MultikeyMap(Map map) {
        this.map = map;
    }

    public Object get(Object... keys) {
        return doGet(map, false, 0, keys);
    }

    public String getString(Object... keys) {
        return doGetString(false, keys);
    }

    public String getNotNullString(Object... keys) {
        return doGetString(true, keys);
    }

    public Long getLong(Object... keys) {
        return doGetNumeric(Number::longValue, false, keys);
    }

    public long getNotNullLong(Object... keys) {
        return doGetNumeric(Number::longValue, true, keys);
    }

    public Double getDouble(Object... keys) {
        return doGetNumeric(Number::doubleValue, false, keys);
    }

    public double getNotNullDouble(Object... keys) {
        return doGetNumeric(Number::doubleValue, true, keys);
    }

    public Boolean getBoolean(Object... keys) {
        return doGetBoolean(false, keys);
    }

    public boolean getNotNullBoolean(Object... keys) {
        return doGetBoolean(true, keys);
    }

    public <T extends Enum<T>> T getEnum(Class<T> enumType, Object... keys) {
        String raw = getString(keys);
        if(raw == null) {
            return null;
        }
        return Enum.valueOf(enumType, raw);
    }

    private Boolean doGetBoolean(boolean notNull, Object[] keys) {
        Object raw = doGet(map, notNull, 0, keys);
        if(raw == null) {
            return null;
        }
        if(raw instanceof Boolean) {
            return (Boolean) raw;
        }
        throw new RuntimeException("value matching to key " + Arrays.toString(keys) + " is not a proper boolean value (" + raw.toString() + ")");
    }

    private <T> T doGetNumeric(Function<Number, T> getter, boolean notNull, Object[] keys) {
        Object raw = doGet(map, notNull, 0, keys);
        if(raw == null) {
            return null;
        }
        if(raw instanceof Number) {
            return getter.apply((Number) raw);
        }
        throw new RuntimeException("value matching to key " + Arrays.toString(keys) + " is not a proper number (" + raw.toString() + ")");
    }

    private String doGetString(boolean notNull, Object[] keys) {
        Object raw = doGet(map, notNull, 0, keys);
        if(raw == null) {
            return null;
        }
        if(!(raw instanceof String)) {
            throw new IllegalArgumentException("Expected String got " + raw);
        }
        return (String)raw;
    }

    private Object doGet(Map searchMap, boolean notNull, int start, Object[] keys) {
        if(keys.length-start == 0) {
            if(notNull) {
                throw new IllegalArgumentException("map(" + Arrays.toString(keys) + ") must not be null");
            }
            return null;
        }
        if(keys.length-start == 1) {
            Object result = searchMap.get(keys[0]);
            if(result == null && notNull) {
                throw new IllegalArgumentException("map(" + Arrays.toString(keys) + ") must not be null");
            }
            return result;
        }
        Object raw = searchMap.get(start);
        if(!(raw instanceof Map)) {
            throw new IllegalArgumentException("expected map but got " + raw.getClass());
        }
        return doGet((Map)raw, notNull, start+1, keys);
    }

    @Override
    public String toString() {
        return map.toString();
    }
}