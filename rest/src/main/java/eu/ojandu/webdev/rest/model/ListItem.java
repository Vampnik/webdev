package eu.ojandu.webdev.rest.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class ListItem {

    @Id
    @GeneratedValue
    private Long id;

    private Long parentItemId;

    private String value;

    @Column
    @Enumerated(EnumType.STRING)
    private ListItemType type;

    private Double x;

    private Double y;

    private Long createdOn;

    private Boolean completed;
}
