#!/bin/bash

java -Djava.security.egd=file:/dev/./urandom \
    -cp app/:lib/* \
    -Dspring.social.facebook.appId=$FB_APP_ID \
    -Dspring.social.facebook.appSecret=$FB_APP_SECRET \
    -Dpublic.host=$PUBLIC_HOST \
    eu.ojandu.webdev.rest.RestRunner